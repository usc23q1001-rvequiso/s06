from abc import ABC, abstractclassmethod

members = []

class Person(ABC):
	@abstractclassmethod
	
	def getFullName():
		pass
	def addRequest():
		pass
	def checkRequest():
		pass
	def addUser():
		pass

class Employee(Person):
	# a. Properties
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def get_firstName(self):
		print(f'First Name: {self._firstName}')

	def get_lastName(self):
		print(f'Last Name: {self._lastName}')

	def get_email(self):
		print(f'Email: {self._email}')

	def get_department(self):
		print(f'Department: {self._department}')

	# b. Methods

	# c. Abstract methods
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return f"Request has been added"

	def checkRequest(self):
		return f"Request has been checked"

	def addUser(self):
		return f"User has been added"
		
	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

class TeamLead(Person):
	# a. Properties
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def get_firstName(self):
		print(f'First Name: {self._firstName}')

	def get_lastName(self):
		print(f'Last Name: {self._lastName}')

	def get_email(self):
		print(f'Email: {self._email}')

	def get_department(self):
		print(f'Department: {self._department}')

	# b. Methods
	def get_members(self):
		return members


	# c. Abstract methods
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return f"Request has been added"

	def checkRequest(self):
		return f"Request has been checked"

	def addUser(self):
		return f"User has been added"

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

	def addMember(self, employee):
		members.append(employee)

class Admin(Person):
	# a. Properties
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	def set_firstName(self, firstName):
		self._firstName = firstName

	def set_lastName(self, lastName):
		self._lastName = lastName

	def set_email(self, email):
		self._email = email

	def set_department(self, department):
		self._department = department

	def get_firstName(self):
		print(f'First Name: {self._firstName}')

	def get_lastName(self):
		print(f'Last Name: {self._lastName}')

	def get_email(self):
		print(f'Email: {self._email}')

	def get_department(self):
		print(f'Department: {self._department}')

	# b. Methods

	# c. Abstract methods
	def getFullName(self):
		return f"{self._firstName} {self._lastName}"

	def addRequest(self):
		return f"Request has been added"

	def checkRequest(self):
		return f"Request has been checked"

	def login(self):
		return f"{self._email} has logged in"

	def logout(self):
		return f"{self._email} has logged out"

	def addUser(self):
		return f"User has been added"

class Request():
	# a. Properties
	def __init__(self, name, requester, dateRequested):
		self.name = name
		self.requester = requester
		self.dateRequested = dateRequested
		self.status = 'Open'

	def set_name(self, name):
		self.name = name

	def set_requester(self, requester):
		self.requester = requester

	def set_dateRequested(self, dateRequested):
		self.dateRequested = dateRequested

	def set_status(self, status):
		self.status = status

	def get_name(self):
		print(f'Name: {self.name}')

	def get_requester(self):
		print(f'Requester: {self.requester}')

	def get_dateRequested(self):
		print(f'Date Requested: {self.dateRequested}')

	def get_status(self):
		print(f'Status: {self.status}')


	# b. Methods
	def updateRequest(self, updatedRequest):
		name = updatedRequest
		return f"Request has beed updated"

	def closeRequest(self):
		return f"{self.name} has been {self.status}"

	def cancelRequest(self):
		def __del__(self):
			print("Request cancelled")






# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"



teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
	print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())

